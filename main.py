import gitlab
import argparse

parser = argparse.ArgumentParser(description="Process username and PAT.")
parser.add_argument("projId", help="Project ID from gitlab: namespace/projectName")
parser.add_argument("pat", help="Your GitLab Personal Access Token")

args = parser.parse_args()


def __main__():
    gl = gitlab.Gitlab(private_token=args.pat)
    project = gl.projects.get(args.projId)
    open_issues = gl.issues.list(state="opened")
    closed_issues = gl.issues.list(state="closed")
    tagged_issues = gl.issues.list(labels=["foo", "bar"])

    for issue in open_issues:
        # print (issue.to_json())
        name = issue.asdict()["title"]
        iid = issue.asdict()["iid"]
        blockCount = issue.asdict()["blocking_issues_count"]
        print(f"Issue: {name} #{iid}")
        print(f"labels: {issue.labels}")
        print(f"blocking issue count: {blockCount}")
        # print(issue.links.list()) Issue links (blocking) not available in gitlab free


__main__()
