# Gitlab-BlockerViz

This script connects to GitLab using a provided Personal Access Token (PAT) and fetches information about issues for a specified project.

## Requirements

- `gitlab`: The Python GitLab library.

## Usage

To run the script, provide the project ID in the format `namespace/projectName` and your GitLab Personal Access Token:

```bash
python script_name.py PROJECT_ID YOUR_PERSONAL_ACCESS_TOKEN
```

For example:

```bash
python script_name.py Rytai/gitlab-blockerviz ABCDE12345TOKEN
```

## Description

Upon execution, the script performs the following operations:

1. Connects to GitLab using the provided PAT.
2. Fetches the specified project using the project ID provided in the command-line argument.
3. Retrieves a list of open issues for the specified project.
   
> Note: The script fetches `closed_issues` and `tagged_issues` as well, but currently does not make use of them.

4. For each open issue, the script prints:
   - Issue title.
   - Issue ID.
   - Labels associated with the issue.
   - Count of blocking issues.

> Note: Direct links to blocking issues (issue links) are not available in GitLab Free.

## Functionality

- **Connect to GitLab**: The script connects to GitLab using the provided PAT.
- **Fetch Project**: The specified project is fetched using its ID.
- **Retrieve Issues**: Lists of open, closed, and tagged issues (with labels `foo` or `bar`) are retrieved.
- **Display Issues**: Information about each open issue, such as its title, ID, associated labels, and blocking issue count, is printed to the console.


## Example output
```
Issue: TestIssueB #2
labels: ['TestLabelA']
blocking issue count: 0
Issue: TestIssueA #1
labels: ['TestLabelA']
blocking issue count: 0
```